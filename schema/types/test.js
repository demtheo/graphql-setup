const {GraphQLObjectType, GraphQLInt} = require('graphql')

const testType = new GraphQLObjectType({
  name: 'Test',
  description: 'A Test type',
  fields: () => ({
    id: {
      type: GraphQLInt,
      description: 'Test ID',
      resolve: (test) => test.id,
    },
  }),
})

exports.testType = testType
