const {GraphQLNonNull, GraphQLInt} = require('graphql')
const {testType} = require('../types/test')
const {getTests} = require('../resolvers/testResolver')

const getTestQuery = () => ({
  type: testType,
  args: {
    queryParam: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve: () => getTests(),
})

exports.getTestQuery = getTestQuery