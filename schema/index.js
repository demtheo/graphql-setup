var {GraphQLSchema, GraphQLObjectType} = require('graphql');

const schemaQueries = ({
  test: require('./queries/test').getTestQuery(),
})

const schemaMutations = ({
  //here write your mutations
})

const Schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: schemaQueries,
  }),
  mutation: new GraphQLObjectType({
    name: 'RootMutations',
    description: 'Available mutations',
    fields: schemaMutations,
  }),
})

exports.default = Schema
